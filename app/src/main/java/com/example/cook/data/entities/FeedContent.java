package com.example.cook.data.entities;

import java.util.ArrayList;

public class FeedContent {
    private String feedTitle;
    private int feedDate;
    private int feedMonth;
    private int feedTime;
    private int feedTimeStart;
    private int feedTimeEnd;
    private int feedVote;
    private int isfeedVote;
    private String feedLocation;
    private String feedImage;

    public FeedContent() {
        this.feedTitle = "This is Feed Title";
        this.feedDate = 1;
        this.feedMonth = 1;
        this.feedTime = 1;
        this.feedTimeStart = 1;
        this.feedTimeEnd = 1;
        this.feedVote = 0;
        this.isfeedVote = 0;
        this.feedLocation = "America";
        this.feedImage = null;
    }

    public FeedContent(String feedTitle, int feedVote, int isfeedVote, String feedImage) {
        this.feedTitle = feedTitle;
        this.feedDate = 1;
        this.feedMonth = 1;
        this.feedTime = 1;
        this.feedTimeStart = 1;
        this.feedTimeEnd = 1;
        this.feedVote = feedVote;
        this.isfeedVote = isfeedVote;
        this.feedLocation = "America";
        this.feedImage = feedImage;
    }

    //region GETTER
    public FeedContent(String feedTitle, int feedDate, int feedMonth, int feedTime, int feedTimeStart, int feedTimeEnd, int feedVote, int isfeedVote, String feedLocation, String feedImage) {
        this.feedTitle = feedTitle;
        this.feedDate = feedDate;
        this.feedMonth = feedMonth;
        this.feedTime = feedTime;
        this.feedTimeStart = feedTimeStart;
        this.feedTimeEnd = feedTimeEnd;
        this.feedVote = feedVote;
        this.isfeedVote = isfeedVote;
        this.feedLocation = feedLocation;
        this.feedImage = feedImage;
    }

    public String getFeedTitle() {
        return feedTitle;
    }

    public void setFeedTitle(String feedTitle) {
        this.feedTitle = feedTitle;
    }

    public int getFeedDate() {
        return feedDate;
    }

    public void setFeedDate(int feedDate) {
        this.feedDate = feedDate;
    }

    public int getFeedMonth() {
        return feedMonth;
    }

    public void setFeedMonth(int feedMonth) {
        this.feedMonth = feedMonth;
    }

    public int getFeedTime() {
        return feedTime;
    }

    public void setFeedTime(int feedTime) {
        this.feedTime = feedTime;
    }

    public int getFeedTimeStart() {
        return feedTimeStart;
    }

    public void setFeedTimeStart(int feedTimeStart) {
        this.feedTimeStart = feedTimeStart;
    }

    public int getFeedTimeEnd() {
        return feedTimeEnd;
    }

    public void setFeedTimeEnd(int feedTimeEnd) {
        this.feedTimeEnd = feedTimeEnd;
    }

    public int getFeedVote() {
        return feedVote;
    }

    public void setFeedVote(int feedVote) {
        this.feedVote = feedVote;
    }

    public int getIsfeedVote() {
        return isfeedVote;
    }

    public void setIsfeedVote(int isfeedVote) {
        this.isfeedVote = isfeedVote;
    }

    public String getFeedLocation() {
        return feedLocation;
    }

    public void setFeedLocation(String feedLocation) {
        this.feedLocation = feedLocation;
    }

    public String getFeedImage() {
        return feedImage;
    }

    public void setFeedImage(String feedImage) {
        this.feedImage = feedImage;
    }
    //endregion

    public static ArrayList<FeedContent> getData(int dataCount){
        ArrayList<FeedContent> feedContents = new ArrayList<>();
        for(int i = 0; i< dataCount; i++){
            FeedContent feedContent= new FeedContent();
            feedContent.setFeedTitle("This is title "+i);
            feedContent.setFeedDate(i%30);
            feedContent.setFeedMonth(i%12);
            feedContent.setFeedLocation("California");
            feedContent.setFeedVote(112);
            feedContent.setFeedImage("https://scontent.fhan4-1.fna.fbcdn.net/v/t1.0-9/p960x960/70617499_484414252420453_2370311348794949632_o.jpg?_nc_cat=104&_nc_eui2=AeFbsRnNYVcjvPjM7SgfavESikqA7QTAwI7burI0-aiLBsDex4i7tLMRbY16ieOW6Fl9HdfylWCgxhhH0UtxYzOZ6HfAte5H8R_OcaOL3Zf1ig&_nc_oc=AQnCn02LcAXm8R1zo13sKRFMAHUb-JCH_tf5GjVusUW7PbgtfZEaiDX00djzzlfMZyo&_nc_ht=scontent.fhan4-1.fna&_nc_tp=6&oh=ace075da2385c1d3d41d7cd5602d466a&oe=5EBA752F");
            feedContent.setFeedTime(2);
            feedContent.setFeedTimeStart(14);
            feedContent.setFeedTimeEnd(17);

            feedContents.add(feedContent);
        }

        return feedContents;
    }
}

