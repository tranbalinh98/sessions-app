package com.example.cook.common;

import java.time.DayOfWeek;

public class FormatString {

    public static String formatFeedVote(int feedVote){
        return feedVote+" Votes";
    }

    public static String fomatFeedTime(int time, int timeStart, int timeEnd){
        return time + " " +timeStart+":am - "+ timeEnd+":pm";
    }
}
