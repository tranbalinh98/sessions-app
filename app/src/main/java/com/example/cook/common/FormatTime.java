package com.example.cook.common;

public class FormatTime {
    public static String getTime(int time){
        if (time<=12){
            return (time / 12) + ":00"+"am";
        }else{
            return (time % 12) + ":00"+"pm";
        }
    }
}
