package com.example.cook.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.cook.R;
import com.example.cook.common.FormatString;
import com.example.cook.common.FormatTime;
import com.example.cook.data.entities.FeedContent;

import java.util.ArrayList;

public class FeedContentAdapter extends RecyclerView.Adapter<FeedContentAdapter.ViewHolder> {

    private ArrayList<FeedContent> feedContents;
    private Context context;

    public FeedContentAdapter() {
    }

    public FeedContentAdapter(Context context) {
        this.context = context;
    }

    public FeedContentAdapter(Context context, ArrayList<FeedContent> feedContents) {
        this.context = context;
        this.feedContents = feedContents;
    }

    @NonNull
    @Override
    public FeedContentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_feed_content,parent, false);
        FeedContentAdapter.ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    public void onBindViewHolder(@NonNull FeedContentAdapter.ViewHolder holder, int position) {
        FeedContent feedContent = feedContents.get(position);
        try{
            holder.tvFeedDate.setText(feedContent.getFeedDate());
            holder.tvFeedTitle.setText(feedContent.getFeedTitle());
            holder.tvFeedLocation.setText(feedContent.getFeedLocation());
            holder.tvFeedTime.setText(FormatString.fomatFeedTime(feedContent.getFeedTime(),feedContent.getFeedTimeStart(),feedContent.getFeedTimeEnd()));
            holder.tvFeedMonth.setText(feedContent.getFeedMonth());
            holder.ivFeedContent.setImageResource(R.drawable.ic_facebook);
//            Glide.with(context).load(feedContent.getFeedImage()).into(holder.ivFeedContent);
        }catch (Exception ex){
            holder.ivFeedContent.setImageResource(R.drawable.ic_facebook);
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return feedContents.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ivFeedContent;
        ImageView ivFeedVote;
        ImageView ivFeedLocation;

        TextView tvFeedVote;
        TextView tvFeedDate;
        TextView tvFeedMonth;
        TextView tvFeedTitle;
        TextView tvFeedTime;
        TextView tvFeedLocation;

        public ViewHolder(@NonNull View view) {
            super(view);

            ivFeedContent = view.findViewById(R.id.iv_feed_content);
            ivFeedVote = view.findViewById(R.id.iv_feed_vote);
            ivFeedLocation = view.findViewById(R.id.iv_feed_location);

            tvFeedVote = view.findViewById(R.id.tv_feed_vote);
            tvFeedDate = view.findViewById(R.id.tv_feed_date);
            tvFeedLocation = view.findViewById(R.id.tv_feed_location);
            tvFeedMonth = view.findViewById(R.id.tv_feed_month);
            tvFeedTitle = view.findViewById(R.id.tv_feed_title);
            tvFeedTime = view.findViewById(R.id.tv_feed_time);
        }
    }
}
