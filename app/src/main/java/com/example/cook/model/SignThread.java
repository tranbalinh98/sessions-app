package com.example.cook.model;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cook.view.CreateAccountFragment;
import com.example.cook.view.CreateProfileFragment;
import com.example.cook.view.FacebookConnectFragment;
import com.example.cook.view.MainActivity;
import com.example.cook.view.NewPasswordFragment;
import com.example.cook.view.SuccessFragment;
import com.example.cook.view.VerificationCodeFragment;
import com.example.cook.view.VerificationPhoneFragment;
import com.example.cook.view.WelcomeBackFragment;

import java.util.Stack;

public class SignThread {
    public  enum SignDirection{
        LoginWhithFacebook,
        LoginWithMail
    }
    public Stack<Fragment> getDirection(SignDirection signDirection){
        Stack<Fragment> directionStack = new Stack<>();
        if (signDirection == SignDirection.LoginWhithFacebook){
            VerificationCodeFragment verificarionCodeFragment = VerificationCodeFragment.newInstance();
            VerificationPhoneFragment verificarionPhoneFragment = VerificationPhoneFragment.newInstance();
            CreateAccountFragment createAccountFragment = CreateAccountFragment.newInstance();
            CreateProfileFragment createprofileFragment = CreateProfileFragment.newInstance();
            SuccessFragment succesFragment = SuccessFragment.newInstance();
            FacebookConnectFragment facebookConnectFragment = FacebookConnectFragment.newInstance();

            directionStack.add(verificarionPhoneFragment);
            directionStack.add(verificarionCodeFragment);
            directionStack.add(createAccountFragment);
            directionStack.add(createprofileFragment);
            directionStack.add(succesFragment);
            directionStack.add(facebookConnectFragment);
        }else if (signDirection == SignDirection.LoginWithMail){
            return directionStack;
        }
        return directionStack;
    }
    public Stack<Fragment> LoginWithFacebook(@Nullable Bundle bundle){
        Stack<Fragment> stack = new Stack<>();
        return stack;
    }
    public Stack<Fragment> Login(@Nullable Bundle bundle){
        Stack<Fragment> stack = new Stack<>();

        WelcomeBackFragment welcomeBackFragment = WelcomeBackFragment.newInstance();
        stack.add(welcomeBackFragment);
        return stack;
    }
    public Stack<Fragment> CreateAccount(@Nullable Bundle bundle){
        Stack<Fragment> stack = new Stack<>();

        VerificationCodeFragment verificarionCodeFragment = VerificationCodeFragment.newInstance();
        VerificationPhoneFragment verificarionPhoneFragment = VerificationPhoneFragment.newInstance();
        CreateAccountFragment createAccountFragment = CreateAccountFragment.newInstance();
        CreateProfileFragment createprofileFragment = CreateProfileFragment.newInstance();
        SuccessFragment succesFragment = SuccessFragment.newInstance();
        FacebookConnectFragment facebookConnectFragment = FacebookConnectFragment.newInstance();

        stack.add(verificarionPhoneFragment);
        stack.add(verificarionCodeFragment);
        stack.add(createAccountFragment);
        stack.add(createprofileFragment);
        stack.add(succesFragment);
        stack.add(facebookConnectFragment);
        return stack;
    }
    public Stack<Fragment> getFocusPasswordThead(@Nullable Bundle bundle){
        Stack<Fragment> stack = new Stack<>();
        VerificationCodeFragment verificarionCodeFragment = VerificationCodeFragment.newInstance();
        VerificationPhoneFragment verificarionPhoneFragment = VerificationPhoneFragment.newInstance();
        NewPasswordFragment newPasswordFragment = NewPasswordFragment.newInstance();
        SuccessFragment successFragment = SuccessFragment.newInstance();

        stack.add(verificarionPhoneFragment);
        stack.add(verificarionCodeFragment);
        stack.add(newPasswordFragment);
        stack.add(successFragment);

        return stack;
    }
    public Stack<Fragment> getThread(int thread, @Nullable Bundle bundle){
        if (thread == MainActivity.LOGIN_THREAD)
            return Login(null);
        else if (thread == MainActivity.LOGIN_FACEBOOK_THREAD)
            return LoginWithFacebook(null);
        else
            return CreateAccount(null);
    }
}