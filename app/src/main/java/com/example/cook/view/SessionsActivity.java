package com.example.cook.view;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cook.R;
import com.example.cook.common.BaseActivity;

public class SessionsActivity extends BaseActivity{

    //region GLOBAL
    private final String TAG = "SessionsActivity";
    int resID = R.id.sessionContainer;
    //endregion
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessions);
        Log.i(TAG,"SessionsAvtivity create success");
        FeedFragment fragment = FeedFragment.newInstance();

        replaceFragment(resID, fragment, true);
    }
}
