package com.example.cook.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.cook.R;
import com.example.cook.model.FeedViewPaperAdapter;
import com.google.android.material.tabs.TabLayout;

public class FeedFragment extends Fragment {

    //region global
    TabLayout tlFeed;
    ViewPager vpFeed;
    //region
    public static FeedFragment newInstance() {

        Bundle args = new Bundle();

        FeedFragment fragment = new FeedFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, container, false);

        tlFeed = view.findViewById(R.id.tl_feed);
        vpFeed = view.findViewById(R.id.vp_feed);

        setViewPaper(vpFeed);
        tlFeed.setupWithViewPager(vpFeed);

        return view;
    }
    //region INITIALIZE
    public void setViewPaper(ViewPager viewPaper){
        FeedViewPaperAdapter adapter = new FeedViewPaperAdapter(getChildFragmentManager());
        adapter.addFragment(FeedTabFragment.newInstance(),"Local");
        adapter.addFragment(FeedTabFragment.newInstance(),"Your Circle");
        adapter.addFragment(FeedTabFragment.newInstance(),"Trending");

        viewPaper.setAdapter(adapter);
    }
    //endregion
}
