package com.example.cook.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.cook.R;

import java.util.Queue;
import java.util.Stack;

public class SignActivity  extends AppCompatActivity {
    //region global
    final String TAG = "Sign Activity";

    FragmentManager fragmentManager;
    boolean isForgotPass = false;
    int threadKey;
    int resID;
    int position =0;
    int positionForgotPass = 0;

    ImageView imgSignBack;
    ImageView imgSignNext;
    //endregion
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        fragmentManager = getSupportFragmentManager();
        Intent intent = getIntent();
        this.threadKey = intent.getIntExtra(MainActivity.KEY_THREAD, MainActivity.CREATE_ACCOUNT_THREAD);

        findView();
        initView(threadKey);
        initEvent();
    }
    //region INIT VIEW
        private void findView(){
            imgSignNext = findViewById(R.id.imgSignNext);
            imgSignBack = findViewById(R.id.imgSignBack);
            resID = R.id.frameSignContainer;
        }
        private void initView(int threadKey){
            if (threadKey == MainActivity.CREATE_ACCOUNT_THREAD){
                VerificationPhoneFragment fragment = VerificationPhoneFragment.newInstance();
                replaceFragment(this.resID, fragment, true);
            }else if(threadKey == MainActivity.LOGIN_THREAD){
                WelcomeBackFragment fragment = WelcomeBackFragment.newInstance();
                replaceFragment(this.resID, fragment, true);
            }
            position++;
        }
        private void initEvent(){
            imgSignBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            imgSignNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getNextFragment(null);
                }
            });
        }


    //endregion

    //region EVENT
    private void getNextFragment(@Nullable Bundle bundle){
        if(!isForgotPass){
            if (this.position<0 || this.position >6){
                this.position =0;
            }

            if (this.threadKey == MainActivity.CREATE_ACCOUNT_THREAD){

                switch (this.position){
                    case 1: {
                        replaceFragment(this.resID, VerificationCodeFragment.newInstance(), true);
                        this.position++;
                        break;
                    }
                    case 2:{
                        replaceFragment(this.resID, CreateAccountFragment.newInstance(), true);
                        this.position++;
                        break;
                    }
                    case 3:{
                        replaceFragment(this.resID, CreateProfileFragment.newInstance(),true);
                        this.position++;
                        break;
                    }
                    case 4:{
                        replaceFragment(this.resID, SuccessFragment.newInstance(),true);
                        this.position++;
                        break;
                    }
                    case 5:{
                        replaceFragment(this.resID, FacebookConnectFragment.newInstance(), true);
                        this.position++;
                        break;
                    }
                    case 6:{
                        Intent intent = new Intent(SignActivity.this, SessionsActivity.class);
//                        if (bundle.isEmpty()){
//                            intent.putExtras(bundle);
//                        }
                        startActivity(intent);
                    }
                }
            }else if(this.threadKey == MainActivity.LOGIN_THREAD || this.threadKey == MainActivity.LOGIN_FACEBOOK_THREAD){
                switch (this.position){
                    case 1:{
                        replaceFragment(this.resID, WelcomeBackFragment.newInstance(), true);
                        this.position++;
                        break;
                    }
                    case 2:{
                        Intent intent = new Intent(SignActivity.this, SessionsActivity.class);
//                        if (bundle.isEmpty(){
//                            intent.putExtras(bundle);
//                        }
                        startActivity(intent);
                    }
                }
            }
        }else {
            switch (this.positionForgotPass){
                case 0: {
                    replaceFragment(this.resID, VerificationPhoneFragment.newInstance(), true);
                    this.positionForgotPass++;
                    break;
                }
                case 1: {
                    replaceFragment(this.resID, VerificationCodeFragment.newInstance(), true);
                    this.positionForgotPass++;
                    break;
                }
                case 2: {
                    replaceFragment(this.resID, NewPasswordFragment.newInstance(), true);
                    this.positionForgotPass++;
                    break;
                }
                case 3: {
                    replaceFragment(this.resID, SuccessFragment.newInstance(), true);
                    this.positionForgotPass++;
                    break;
                }
                case 4:{
                    Intent intent = new Intent(SignActivity.this, SessionsActivity.class);
//                    if (bundle.isEmpty()){
//                        intent.putExtras(bundle);
//                    }
                    startActivity(intent);
                }
            }
        }
    }
    private void getBackFragment(){
        onBackPressed();
    }
    //endregion

    //region FRAGMENT UTIL

    protected void addFragment(int resId, Fragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(resId, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        if (addToBackStack) {
            ft.addToBackStack(fragment.getClass().getSimpleName());
        }
        ft.commit();
    }

    protected void replaceFragment(int resId, Fragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(resId, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        if (addToBackStack) {
            ft.addToBackStack(fragment.getClass().getSimpleName());
        }
        ft.commit();
    }
    //endregion


    @Override
    public void onBackPressed() {
        if (!isForgotPass){
            position--;
        }
        else if(this.isForgotPass){
            if(positionForgotPass>0){
                positionForgotPass--;
            }
            else {
                isForgotPass = false;
            }
        }
        super.onBackPressed();
    }
}
