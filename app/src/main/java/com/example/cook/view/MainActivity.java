package com.example.cook.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.cook.R;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MainActivity";

    //global
    RelativeLayout layoutLoginFacebook;
    RelativeLayout layoutLoginPhone;
    TextView tvLogin;

    //final
    public static final int CREATE_ACCOUNT_THREAD = 1;
    public static int LOGIN_FACEBOOK_THREAD = 2;
    public static int LOGIN_THREAD =0;
    public static String KEY_THREAD = "thread";
    public static String KEY_PHONE_NUMBER = "phoneNumber";
    public static String KEY_EMAIL_ADDRESS = "emailAddress";
    public static String KEY_PASSWORD = "password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Log.i(TAG, "MainActicity create success");
        tvLogin = findViewById(R.id.tvLogin);
        layoutLoginFacebook = findViewById(R.id.layoutSignWithFacebook);
        layoutLoginPhone = findViewById(R.id.layoutSignWithPhoneOrEmail);

        final Bundle bundle = new Bundle();
        layoutLoginPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putInt(KEY_THREAD, CREATE_ACCOUNT_THREAD);
                getSiginActivity(null,CREATE_ACCOUNT_THREAD);
            }
        });
        layoutLoginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bundle.putInt(KEY_THREAD, LOGIN_FACEBOOK_THREAD);
            }
        });
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bundle.putInt(KEY_THREAD, LOGIN_THREAD);
                getSiginActivity(null,LOGIN_THREAD);
            }
        });

    }
    private void getSiginActivity(@Nullable Bundle bundle, int keyThread){
        Intent intent = new Intent(MainActivity.this, SignActivity.class);
//        if (!bundle.isEmpty()){
////            intent.putExtras(bundle);
//        }
//        intent.putExtra("",bundle);
        intent.putExtra(KEY_THREAD, keyThread);
        startActivity(intent);
    }
}
