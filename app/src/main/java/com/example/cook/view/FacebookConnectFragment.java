package com.example.cook.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cook.R;

public class FacebookConnectFragment extends Fragment {
    private final String TAG = "FacebookConnectFragment";
    public static FacebookConnectFragment newInstance() {

        Bundle args = new Bundle();

        FacebookConnectFragment fragment = new FacebookConnectFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_facebook_connect,container,false);
        Log.i(TAG, "FacebookConnectFragment create success");

        return view;
    }
}
