package com.example.cook.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cook.R;

public class CreateProfileFragment extends Fragment {
    private final String TAG = "CreateProfileFragment";
    public static CreateProfileFragment newInstance() {
        
        Bundle args = new Bundle();
        
        CreateProfileFragment fragment = new CreateProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_profile,container, false);
        Log.i(TAG, "CreatProfilrFragment create success");

        return view;       
    }
}
