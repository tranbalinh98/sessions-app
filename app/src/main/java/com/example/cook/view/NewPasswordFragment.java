package com.example.cook.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cook.R;

public class NewPasswordFragment extends Fragment {
    private final String TAG = "NewPasswordFragment";
    public static NewPasswordFragment newInstance() {

        Bundle args = new Bundle();

        NewPasswordFragment fragment = new NewPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reset_password,container,false);
        Log.i(TAG, "NewPasswordFragment create success");

        return view;
    }
}
