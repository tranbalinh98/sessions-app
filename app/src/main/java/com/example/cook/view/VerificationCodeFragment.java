package com.example.cook.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cook.R;

public class VerificationCodeFragment extends Fragment {
    public static VerificationCodeFragment newInstance (){
        Bundle args = new Bundle();
        VerificationCodeFragment fragment = new VerificationCodeFragment();
        fragment.setArguments(args);

        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verification_code,container,false);

        return view;
    }
}
