package com.example.cook.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cook.R;

public class VerificationPhoneFragment extends Fragment {

    public static VerificationPhoneFragment newInstance() {

        Bundle args = new Bundle();

        VerificationPhoneFragment fragment = new VerificationPhoneFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verification_phone, container, false);

        return view;
    }
}
