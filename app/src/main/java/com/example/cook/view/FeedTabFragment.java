package com.example.cook.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cook.R;
import com.example.cook.data.entities.FeedContent;
import com.example.cook.model.FeedContentAdapter;

import java.util.ArrayList;

public class FeedTabFragment extends Fragment {

    //region GLOBAL
    RecyclerView rvFragmentTab;
    ArrayList<FeedContent> feedContents;
    RecyclerView rvRecomment;
    //endregion
    public static FeedTabFragment newInstance() {

        Bundle args = new Bundle();

        FeedTabFragment fragment = new FeedTabFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_tab, container, false);
        rvFragmentTab = view.findViewById(R.id.rv_feed_content);
        rvRecomment = view.findViewById(R.id.rv_recommend);
        rvRecomment.setVisibility(View.GONE);
        this.feedContents = FeedContent.getData(10);
        setContentView(feedContents);

        return view;
    }
    //region INTIALIZE
    private void setContentView(ArrayList<FeedContent> feedContents ){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(),1, RecyclerView.VERTICAL,false);
        FeedContentAdapter adapter = new FeedContentAdapter(getContext(),feedContents);
        this.rvFragmentTab.setLayoutManager(layoutManager);
        this.rvFragmentTab.setAdapter(adapter);
    }
    //endregion
}
